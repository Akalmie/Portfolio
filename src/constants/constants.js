export const projects = [
  {
    title: 'Portfolio',
    description:"En tant que developpeur, je ne pouvais pas rester sans portfolio alors j'ai décidé d'en faire. Et comme j'en suis fière, je vous le montre.",
    image: '/images/2.png',
    tags: ['Next.js', 'JavaScript'],
    source: 'https://gitlab.com/Akalmie/Portfolio',
    visit: 'https://google.com',
    id: 0,
  },
  {
    title: 'NatureEmoi',
    description: "Création d'un site vitrine pour une entreprise fictive afin de présenter certaines plantes. ", 
    image: '/images/1.png',
    tags: ['HTML', 'CSS'],
    source: 'https://gitlab.com/Akalmie/natureemoi',
    visit: 'https://google.com',
    id: 1,
  },
  {
    title: 'Application Météo',
    description: "Une application météo qui sert d'une API https://openweathermap.org afin de récupérer notre localisation et d'afficher le temps ainsi que d'autres informations. Comme les données renvoyé en format JSON sont en anglais, mon application l'est aussi",
      image: '/images/3.jpg',
      tags: ['React.js','Javascript'],
    source: 'https://gitlab.com/Akalmie/meteo',
    visit: 'https://google.com',
    id: 2,
  },
  {
    title: 'Montre',
    description: "Création d'une montre affichant la date et l'heure et possédant un thème jour/nuit",
    image: '/images/4.jpg',
    tags: ['HTML', 'CSS', 'JAVASCRIPT'],
    source: 'https://gitlab.com/Akalmie/clock',
    visit: 'https://google.com',
    id: 3,
  },
];

export const TimeLineData = [
  { year: 2015, text: 'Début de mon voyage en intégrant un BTS Système Numérique et Réseau', },
  { year: 2016, text: 'Première expérience dans le monde du travail en tant que développeur chez ATOS et le client BIOGARAN', },
  { year: 2017, text: 'Une grande éxpérience en tant que developpeur back-end chez JIM INFORMATIQUE', },
  { year: 2018, text: 'Projet incroyable de fin de mon BTS -> Création de la poubelle connectée ', },
 // { year: 2019-2021, text: 'Césure des mes études pour entrer dans le monde du travail', },
  { year: 2021, text: 'Intégration du Bachelor 3 de développeur full stack à MDS ', },
  
];